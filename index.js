const express = require('express');
const app = express();
const port = 4000;

app.use(express.json());

let users = [
    {
        name: "John",
        age: 18,
        username:"johnsmith99"
    },
    {
        name: "Johnson",
        age: 21,
        username: "johnson1991"
    },
    {
        name: "Smith",
        age: 19,
        username: "smithMike12"
    }
];

app.get('/users',(req,res)=>{

    return res.send(users);

});

let products = [
    {
        name: "Orange Juice",
        price: 50,
        isActive: true
    },
    {
        name: "Apple Juice",
        price: 55,
        isActive: true
    },
    {
        name: "Grape Juice",
        price: 60,
        isActive: true
    }
];

app.get("/products",(req,res)=>{
    return res.send(products);
})

app.post('/users',(req,res)=>{

    //add a simple if statement that if the request body does not have a name property, we will send an error message along with a 400 http status code (Bad request)
    //.hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object.
    if(!req.body.hasOwnProperty("name")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter NAME"
        })
    }

    if(!req.body.hasOwnProperty("age")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter AGE"
        })
    }

    if(!req.body.hasOwnProperty("username")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter USERNAME"
        })
    }

})


app.listen(port,()=>console.log(`Server is running at localhost:${port}`));
